from keras.models import Sequential
from keras.layers import Dense
from sklearn.cross_validation import train_test_split
import numpy

# fix random seed for reproducibility
seed = 7
numpy.random.seed(seed)

# load dataset
dataset = numpy.loadtxt("Xy.csv", delimiter=",")

# split into input (X) and output (Y) variables
X = dataset[:,0:15459]
Y = dataset[:,15459:15472]
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=seed)

# create model
model = Sequential()
model.add(Dense(500, input_dim=15459, init='uniform', activation='sigmoid'))
model.add(Dense(100, init='uniform', activation='sigmoid'))
model.add(Dense(13, init='uniform', activation='sigmoid'))

# Compile model
model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

# Fit the model
model.fit(X_train, y_train, nb_epoch=200, batch_size=2000,  verbose=2)

# calculate predictions

scores = model.evaluate(X_test, y_test, verbose=2)
print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))
