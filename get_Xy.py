import requests, json, re, sys
from sys import argv
from nltk.corpus import stopwords

#############
### VARIABLES
#############
stoplist = stopwords.words('english')
Words=[]

Emotions=['anger','boredom','empty','enthusiasm','fun','happiness','hate','love','neutral','relief','sadness','surprise','worry']

texts_file=argv[1]
dictionary_file=argv[2]


TO_SKIP=["be", "more", "most", "really", "quite", "should", "very", "must", "not",  "no",  "less",  "too", "have", "a bit"]

################################################ 
# Functions to get different kinds of analysis #
################################################

# For every noun/adj/adv or verb in text, their
# corresponding POS and lemma
def get_pos_lemmas(analysis):
    analysis=json.loads(analysis)    
    pos_lemmas=[]
    for sentence in analysis:
        for word in sentence:
            lemma=word.get('lemma')
            if lemma not in stoplist:
                pos=word.get('pos')
                match=re.match('^(noun|adjective|verb|adverb)$',pos)
                if match:
                    pos_lemma=lemma + "#" + pos
                    pos_lemmas.append(pos_lemma)
    return pos_lemmas

# Pairs topic/opinion found in text
def get_sentiment_pairs(analysis):
    if not analysis:
        analysis="[]"

    analysis=json.loads(analysis)
    sentiment_pairs=[]
    for sentence in analysis:
        score=float(sentence.get('score'))
        topic=sentence.get('topic_norm')
        polarity=None
        if score > 0:
            polarity='positive'
        elif score < 0:
            polarity='negative'
            
        if topic != "" and polarity:
            text=sentence.get('text_norm')
            topics=topic.split(',')
            texts=text.split(',')
            for tp in topics:
                for tx in texts:
                    if tx not in TO_SKIP:
                        sentiment_pair= "#".join([tp,tx,polarity])
                        sentiment_pairs.append(sentiment_pair)
                    
    return sentiment_pairs

# Raw words in text with minimal normalization
# lower case, punctuation deletion

def normalize_word(word):
    word=word.lower()
    word = re.sub("^\s*[,.?!'\"]+", "", word)
    word = re.sub("[,.?!'\"]+\s*$", "", word)
    return word

def get_normalized_words(text):
    words=text.split(' ')
    normalized_words=[]
    for word in words:
        normalized_word=normalize_word(word)
        normalized_words.append(normalized_word)
    return normalized_words

############
### MAIN ###
############

# The features dictionary in stored in Words 
with open(dictionary_file) as f:
    for line in f:
        line = line.strip('\n')
        fields=line.split('\t')
        word=fields[0]
        Words.append(word)

# For every tweet in the dataset, we extract their
# features and according to Words, we get a vectorized
# representation

with open(texts_file) as f:
    for line in f:
        line = line.strip('\n')
        fields=line.split('\t')
        if len(fields) > 5:
            text=fields[3]
            analysis_pos_lemma=fields[4]
            analysis_sentiment=fields[5]
            emotion=fields[1]
            features=[]

            for f in get_pos_lemmas(analysis_pos_lemma):
                features.append(f)
            for f in get_sentiment_pairs(analysis_sentiment):
                features.append(f)

            X=[]
            for word in Words:
                if word in features:
                    X.append(1)
                else:
                    X.append(0)
            y=[]
            for e in Emotions:
                if e == emotion:
                    y.append(1)
                else:
                    y.append(0)
            Xstring=','.join([str(x) for x in X])
            ystring=','.join([str(i) for i in y])
            print(Xstring+','+ystring)
