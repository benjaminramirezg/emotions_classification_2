import requests, json, re
from sys import argv
import sys,json, logging
logging.basicConfig(level=logging.INFO)
#############
### VARIABLES
#############

texts_file=argv[1]

# For API

def ask_api(text):
    oauth_token = '8386dc9b23144166a0a09ac0326278a4'
    endpoint = "https://svc02.api.bitext.com/sentiment/"
    headers = { "Authorization" : "bearer " + oauth_token, "Content-Type" : "application/json" }
    params = {"language" : "eng","text" : text }
    res = requests.post(endpoint, headers=headers, data=json.dumps(params))
    resultid = json.loads(res.text).get('resultid')
    analysis = []

    for i in range(1,100):
        res = requests.get(endpoint + resultid + '/', headers=headers)
        if res.status_code == 200 :
            analysis= json.loads(res.text).get('sentimentanalysis')
            break
    return analysis

count=0
with open(texts_file) as f:
    for line in f:
        count = count + 1
        logging.info('Iteration: '+str(count))
        try:
            line = line.strip('\n')
            fields=line.split('\t')
            text=fields[3]
            features=ask_api(text)
            print(line + "\t" + json.dumps(features))
        except:
            print(line + "\t[]")
